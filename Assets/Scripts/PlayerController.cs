﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;
    void Awake() => instance = this;


    public GameObject player;
    private Rigidbody2D playerRb;
    public BoxCollider2D boxCollider2d;
    private Animator animator;
    private GameObject riflePivot;
    public Animator rifleAnimator;

    [SerializeField] private LayerMask platformLayerMask;
    public float moveSpeed = 15.0f;
    public float jumpHeight = 10.0f;

    private bool isCrouching;
    private bool isFacingRight;
    public bool gunMode;

    public Transform firePoint;
    public GameObject bulletPrefab;
    public float bulletForce = 20f;

    public bool collidedTemporaryPlatform;
    private float acumTime = 0f;
    private float mouseAcumTime = 0f;
    private float holdTime = 0.4f;
    private float timeRemaining = 1.0f;
    private bool timerIsRunning = false;

    void Start()
    {
        playerRb = GetComponent<Rigidbody2D>();
        animator = GetComponentInChildren<Animator>();
        boxCollider2d = GetComponent<BoxCollider2D>();
        SwipeDetector.OnSwipe += SwipeDetector_OnSwipe;

        isCrouching = false;
        isFacingRight = true;
        gunMode = true;
        riflePivot = player.transform.Find("RiflePivot").gameObject;
        rifleAnimator = player.transform.Find("RiflePivot").GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.acceleration.x != 0f || Input.GetAxis("Horizontal") != 0)
        {
            animator.SetBool("isRunning", true);
            if (Input.acceleration.x > 0.1f || Input.GetAxis("Horizontal") > 0)
            {
                if (!isFacingRight && !timerIsRunning)
                    TurnPlayer();
                float horizontalMove = 1.0f;
                Vector2 movement = new Vector2(horizontalMove, 0.0f);
                transform.position = transform.position + new Vector3(horizontalMove * moveSpeed * Time.deltaTime, 0, 0);
            }
            else if (Input.acceleration.x < -0.1f || Input.GetAxis("Horizontal") < 0)
            {
                if (isFacingRight && !timerIsRunning)
                    TurnPlayer();
                float horizontalMove = -1.0f;
                Vector2 movement = new Vector2(horizontalMove, 0.0f);
                transform.position = transform.position + new Vector3(horizontalMove * moveSpeed * Time.deltaTime, 0, 0);
            }
        }
        else
        {
            animator.SetBool("isRunning", false);
        }

        if (IsGrounded())
        {
            animator.SetBool("isJumping", false);
        }
        else
        {
            animator.SetBool("isJumping", true);
        }

        if (gunMode)
        {
            riflePivot.SetActive(true);
            if (Input.touchCount > 0 || Input.GetMouseButtonDown(0))
            {
                //acumTime += Input.GetTouch(0).deltaTime;
                //if (Input.GetTouch(0).phase == TouchPhase.Ended || acumTime <= holdTime)
                //{
                //Vector3 difference = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position) - riflePivot.transform.position;
                Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - riflePivot.transform.position;
                difference.Normalize();
                float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
                if (isFacingRight)
                {
                    if (rotationZ < 70f && rotationZ > -45f)
                    {
                        riflePivot.transform.rotation = Quaternion.Euler(0f, 0f, rotationZ);
                        rifleAnimator.SetTrigger("Shot");
                        Shoot();
                    }
                    else if (rotationZ < -135f || rotationZ > 115f)
                    {
                        TurnPlayer();
                        riflePivot.transform.rotation = Quaternion.Euler(180f, 180f, rotationZ);
                        rifleAnimator.SetTrigger("Shot");
                        Shoot();
                    }
                }
                else if (!isFacingRight)
                {
                    if (rotationZ < 70f && rotationZ > -45f)
                    {
                        TurnPlayer();
                        riflePivot.transform.rotation = Quaternion.Euler(0f, 0f, rotationZ);
                        rifleAnimator.SetTrigger("Shot");
                        Shoot();
                    }
                    else if (rotationZ < -135f || rotationZ > 115f && !isFacingRight)
                    {
                        riflePivot.transform.rotation = Quaternion.Euler(180f, 180f, rotationZ);
                        rifleAnimator.SetTrigger("Shot");
                        Shoot();
                    }
                }
                acumTime = 0f;
                //}
                if (Input.GetTouch(0).phase == TouchPhase.Ended && acumTime >= holdTime)
                {
                    acumTime = 0f;
                }
            }
        }
        else if (!gunMode)
        {
            riflePivot.SetActive(false);
        }

        if (timerIsRunning)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                //Debug.Log(timeRemaining);
            }
            else
            {
                //Debug.Log("Time has run out!");
                timeRemaining = 1.0f;
                timerIsRunning = false;
            }
        }

        if (Input.GetKeyDown("space") && IsGrounded())
        {
            playerRb.velocity = Vector2.up * jumpHeight;
            animator.SetTrigger("takeOff");
        }
        if (Input.GetKey("s") && isCrouching == false && !collidedTemporaryPlatform)
        {
            animator.SetBool("isCrouching", true);
            moveSpeed = 5.0f;
            isCrouching = true;
        }
        else if (Input.GetKey("s") && isCrouching != false && !collidedTemporaryPlatform)
        {
            animator.SetBool("isCrouching", false);
            moveSpeed = 15.0f;
            isCrouching = false;
        }
    }

    void TurnPlayer()
    {
        Vector3 characterScale = transform.localScale;
        bool playerTurned = false;
        if (isFacingRight && !playerTurned)
        {
            characterScale.x = -1f;
            isFacingRight = false;
            playerTurned = true;
        }
        else if (!isFacingRight && !playerTurned)
        {
            characterScale.x = 1f;
            isFacingRight = true;
        }
        transform.localScale = characterScale;
        playerTurned = false;
    }

    public void OnCollisionStay2D(Collision2D other)
    {
        CheckForTempPlatform(other);
    }

    public void OnCollisionExit2D(Collision2D other)
    {
        collidedTemporaryPlatform = false;
    }

    public void SwipeDetector_OnSwipe(SwipeData data)
    {
        if (data.Direction == SwipeDirection.Up || Input.GetKeyDown("space") && IsGrounded())
        {
            Jump();
        }
        if (data.Direction == SwipeDirection.Down && isCrouching == false && !collidedTemporaryPlatform)
        {
            CrouchModeOn();
        }
        else if (data.Direction == SwipeDirection.Down && isCrouching != false && !collidedTemporaryPlatform)
        {
            CrouchModeOff();
        }
        if (data.Direction == SwipeDirection.Down && collidedTemporaryPlatform)
        {
            DisablePlatform();
        }
    }

    public bool IsGrounded()
    {
        float extraHeightText = 2f;
        RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider2d.bounds.center, boxCollider2d.bounds.size, 0f, Vector2.down, extraHeightText, platformLayerMask);
        Color rayColor;
        if (raycastHit.collider != null)
        {
            rayColor = Color.green;
        }
        else
        {
            rayColor = Color.red;
        }
        Debug.DrawRay(boxCollider2d.bounds.center + new Vector3(boxCollider2d.bounds.extents.x, 0), Vector2.down * (boxCollider2d.bounds.extents.y + extraHeightText), rayColor);
        Debug.DrawRay(boxCollider2d.bounds.center - new Vector3(boxCollider2d.bounds.extents.x, 0), Vector2.down * (boxCollider2d.bounds.extents.y + extraHeightText), rayColor);
        Debug.DrawRay(boxCollider2d.bounds.center - new Vector3(boxCollider2d.bounds.extents.x, boxCollider2d.bounds.extents.y + extraHeightText), Vector2.right * (boxCollider2d.bounds.extents.x) * 2, rayColor);
        //Debug.Log(raycastHit.collider);
        return raycastHit.collider != null;
    }

    void Shoot()
    {
        if (isFacingRight)
        {
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(firePoint.right * bulletForce, ForceMode2D.Impulse);
        }
        else if (!isFacingRight)
        {
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            Vector3 bulletScale = bullet.transform.localScale;
            bulletScale.x = -1f;
            bullet.transform.localScale = bulletScale;
            rb.AddForce(-firePoint.right * bulletForce, ForceMode2D.Impulse);
        }
        timerIsRunning = true;
    }
    void Jump()
    {
        playerRb.velocity = Vector2.up * jumpHeight;
        animator.SetTrigger("takeOff");
    }
    void CrouchModeOn()
    {
        animator.SetBool("isCrouching", true);
        moveSpeed = 5.0f;
        isCrouching = true;
    }
    void CrouchModeOff()
    {
        animator.SetBool("isCrouching", false);
        moveSpeed = 15.0f;
        isCrouching = false;
    }
    void DisablePlatform()
    {
        GameObject.FindWithTag("TemporaryPlatform").GetComponent<Collider2D>().enabled = false;
        GameObject.FindWithTag("TemporaryPlatform").GetComponent<PolygonCollider2D>().enabled = false;
    }
    void CheckForTempPlatform(Collision2D other)
    {
        if (other.gameObject.tag == "TemporaryPlatform")
        {
            collidedTemporaryPlatform = true;
        }
        else
        {
            collidedTemporaryPlatform = false;
        }
    }
}
