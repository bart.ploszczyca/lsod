﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private float bulletLifeSpawn = 2.0f;
    private void Update()
    {
        BulletTimer();
    }
    void BulletTimer()
    {
        if (bulletLifeSpawn > 0)
        {
            bulletLifeSpawn -= Time.deltaTime;
            //Debug.Log(runnningTime);
        }
        else
        {
            //Debug.Log("Time has run out!");
            Destroy(gameObject);
            }
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
    }
}
