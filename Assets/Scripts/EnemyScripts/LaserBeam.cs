﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBeam : MonoBehaviour
{
    private LineRenderer lineRenderer;
    public Transform laserHit;
    private Transform target;
    public float laserLifeSpawn;

    private bool calculatedTargetPos = false;
    private Vector2 targetPos;
    LayerMask layerMask = ~(1 << 9) & ~(1 << 10);

    // Start is called before the first frame update
    void Start()
    {
        //layerMask = LayerMask.GetMask("Player");
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.enabled = true;
        lineRenderer.useWorldSpace = true;
    }

    // Update is called once per frame
    void Update()
    {        
        var target = PlayerController.instance;
        Vector2 playerPos = target.gameObject.transform.position;
        Vector2 laserPos = gameObject.transform.position;

        if (!calculatedTargetPos)
        {
            targetPos = playerPos - laserPos;
            calculatedTargetPos = true;
        }

        RaycastHit2D hit = Physics2D.Raycast(transform.position, targetPos, Mathf.Infinity, layerMask);
        Debug.DrawLine(transform.position, hit.point);
        laserHit.position = hit.point;
        lineRenderer.SetPosition(0, transform.position);
        lineRenderer.SetPosition(1, laserHit.position);
        laserLifeSpawn -= Time.deltaTime;

        if(laserLifeSpawn < 0)
        {
            Destroy(gameObject);
        }
    }
}
