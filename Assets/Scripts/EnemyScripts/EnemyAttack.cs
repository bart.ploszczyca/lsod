﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : StateMachineBehaviour
{
    private Transform target;

    public float speed;
    public float stoppingDistance;
    public float retreatDistance;

    public Transform LBSpawnPoint;
    private float timeBtwShots;
    public float startTimeBtwShots;

    public GameObject projectile;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        LBSpawnPoint = animator.transform.GetChild(2).transform;
        timeBtwShots = startTimeBtwShots;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        AttackMovement(animator);
        AttackFrequency(animator);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    void AttackMovement(Animator animator)
    {
        if (Vector2.Distance(animator.transform.position, target.position) > stoppingDistance)
        {
            animator.transform.position = Vector2.MoveTowards(animator.transform.position, target.position, speed * Time.deltaTime);
        }
        else if (Vector2.Distance(animator.transform.position, target.position) < stoppingDistance && Vector2.Distance(animator.transform.position, target.position) > retreatDistance)
        {
            animator.transform.position = animator.transform.position;
        }
        else if (Vector2.Distance(animator.transform.position, target.position) < retreatDistance)
        {
            animator.transform.position = Vector2.MoveTowards(animator.transform.position, target.position, -speed * Time.deltaTime);
        }
    }
    void AttackFrequency(Animator animator)
    {
        if (timeBtwShots <= 0)
        {
            Instantiate(projectile, LBSpawnPoint.position, Quaternion.identity);
            timeBtwShots = startTimeBtwShots;
        }
        else
        {
            timeBtwShots -= Time.deltaTime;
        }
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
