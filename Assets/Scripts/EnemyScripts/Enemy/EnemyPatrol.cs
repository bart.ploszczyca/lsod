﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : StateMachineBehaviour
{
    private Transform pathDetection;
    int layerMask;

    public float patrolSpeed;
    public float pathDetectionDistance;
    
    private bool firstCall = true;

    private bool timerIsRunning = false;
    public float waitingTime = 2.0f;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        layerMask = LayerMask.GetMask("EnemyPatrolPath");
        pathDetection = animator.transform.GetChild(0);
        animator.ResetTrigger("reachedPatrolPath");
        animator.ResetTrigger("playerNotFound");
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Patrol(animator);
        PathDetector();
        WaitingTimer(animator);
    }

    void Patrol(Animator animator)
    {
        if (!timerIsRunning)
        {
            animator.transform.Translate(Vector2.right * patrolSpeed * Time.deltaTime);
        }
    }
    void PathDetector()
    {
        RaycastHit2D pathInfo = Physics2D.Raycast(pathDetection.position, Vector2.down, pathDetectionDistance, layerMask);
        if (pathInfo.collider == false && firstCall)
        {
            timerIsRunning = true;
            firstCall = false;
        }
    }
    void WaitingTimer(Animator animator)
    {
        if (timerIsRunning)
        {
            if (waitingTime > 0)
            {
                waitingTime -= Time.deltaTime;
                //Debug.Log(timeRemaining);
            }
            else
            {
                //Debug.Log("Time has run out!");
                waitingTime = 2.0f;
                timerIsRunning = false;
                TurnEnemy(animator);
            }
        }
    }
    void TurnEnemy(Animator animator)
    {
        if (animator.GetBool("isFacingRight"))
        {
            animator.transform.eulerAngles = new Vector3(0, -180, 0);
            animator.SetBool("isFacingRight", false);
        }
        else if (!animator.GetBool("isFacingRight"))
        {
            animator.transform.eulerAngles = new Vector3(0, 0, 0);
            animator.SetBool("isFacingRight", true);
        }
        firstCall = true;
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
