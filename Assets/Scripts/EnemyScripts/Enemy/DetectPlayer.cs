﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectPlayer : MonoBehaviour
{
    private Animator enemyAnimator;

    private PolygonCollider2D collider;
    private bool colliderTriggered;
    
    private Vector2 eyePos;
    private Vector2 targetPos;
    private LayerMask platformLayerMask = 1 << 8;

    public float seeDistance;

    void Start()
    {
        enemyAnimator = gameObject.GetComponentInParent<Animator>();
        collider = gameObject.GetComponent<PolygonCollider2D>();
    }

    void Update()
    {
        var target = PlayerController.instance;
        targetPos = target.gameObject.transform.position;

        if (colliderTriggered)
        {
            if (targetPos.x > enemyAnimator.transform.position.x && enemyAnimator.GetBool("isFacingRight"))
            {
                EnemyLinesOfSight();
            }
            else if (targetPos.x < enemyAnimator.transform.position.x && !enemyAnimator.GetBool("isFacingRight"))
            {
                EnemyLinesOfSight();
            }
        }
        if (enemyAnimator.GetBool("playerDetected") == false)
        {
            collider.enabled = true;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            enemyAnimator.SetBool("playerDetected", true);
            collider.enabled = false;
            colliderTriggered = true;
        }
    }

    void EnemyLinesOfSight()
    {
        var target = PlayerController.instance;
        targetPos = target.gameObject.transform.position;
        eyePos = this.transform.position;

        Vector2 playerOffset = new Vector2(0, 3.5f);
        Vector2 targetPosUpper = targetPos + playerOffset;
        Vector2 targetPosLower = targetPos - playerOffset;

        Debug.DrawLine(eyePos, targetPosUpper, Color.red);
        Debug.DrawLine(eyePos, targetPosLower, Color.red);

        var upper = Physics2D.Linecast(eyePos, targetPosUpper, 1 << LayerMask.NameToLayer("Platform"));
        var lower = Physics2D.Linecast(eyePos, targetPosLower, 1 << LayerMask.NameToLayer("Platform"));

        if (Vector2.Distance(enemyAnimator.transform.position, targetPos) < seeDistance)
        {
            if (upper.collider != null && lower.collider != null)
            {
                //Debug.Log("kurwa schował się");
                enemyAnimator.SetBool("inSightOfView", false);
                enemyAnimator.SetBool("playerDetected", false);
                collider.enabled = true;
                colliderTriggered = false;
            }
            else
            {
                enemyAnimator.SetBool("inSightOfView", true);
            }
        }
        else
        {
            enemyAnimator.SetBool("playerDetected", false);
            collider.enabled = true;
            colliderTriggered = false;
        }
    }
}
