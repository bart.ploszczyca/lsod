﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGetBack : StateMachineBehaviour
{
    private Transform pathDetection;
    int layerMask;

    public float pathDetectionDistance;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        layerMask = LayerMask.GetMask("EnemyPatrolPath");
        pathDetection = animator.transform.GetChild(0);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        PathDetector(animator);
    }

    void PathDetector(Animator animator)
    {
        RaycastHit2D pathInfo = Physics2D.Raycast(pathDetection.position, Vector2.down, pathDetectionDistance, layerMask);
        if (pathInfo.collider == true)
        {
            animator.SetTrigger("reachedPatrolPath");
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
