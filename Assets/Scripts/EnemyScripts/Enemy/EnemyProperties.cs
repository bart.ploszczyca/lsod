﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProperties : MonoBehaviour
{
    private float enemyHealth;
    

    void Start()
    {
        //var a = PlayerController.instance;

        enemyHealth = 14.0f;
    }
    void Update()
    {
        DestroyWhenKilled();
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Projectile")
        {
            TakeDamage();
        }
    }
    void TakeDamage()
    {
        enemyHealth -= 1.0f;
    }
    void DestroyWhenKilled()
    {
        if(enemyHealth < 0)
        {
            Destroy(gameObject);
        }
    }
}
