﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChase : StateMachineBehaviour
{
    public float chaseSpeed;
    public float attackRange;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    //override public void onstateenter(animator animator, animatorstateinfo stateinfo, int layerindex)
    //{   
    //}

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Chase(animator);
        CheckDistance(animator);
    }
    void Chase(Animator animator)
    {
        var target = PlayerController.instance;
        Vector2 targetPos = target.gameObject.transform.position;

        if (targetPos.x > animator.transform.position.x)
        {
            animator.transform.Translate(Vector2.right * chaseSpeed * Time.deltaTime);
            TurnEnemyIfFacingWrong(animator);
        }
        else
        {
            animator.transform.Translate(Vector2.right * chaseSpeed * Time.deltaTime);
            TurnEnemyIfFacingWrong(animator);
        }
    }
    void CheckDistance(Animator animator)
    {
        var target = PlayerController.instance;
        Vector2 targetPos = target.gameObject.transform.position;

        float distance = Vector2.Distance(targetPos, animator.transform.position);
        if (distance <= attackRange)
        {
            animator.SetBool("inAttackRange", true);
        }
    }
    void TurnEnemyIfFacingWrong(Animator animator)
    {
        var target = PlayerController.instance;
        Vector2 targetPos = target.gameObject.transform.position;

        if (targetPos.x > animator.transform.position.x && !animator.GetBool("isFacingRight"))
        {
            animator.transform.eulerAngles = new Vector3(0, 0, 0);
            animator.SetBool("isFacingRight", true);
        }
        else if(targetPos.x < animator.transform.position.x && animator.GetBool("isFacingRight"))
        {
            animator.transform.eulerAngles = new Vector3(0, -180, 0);
            animator.SetBool("isFacingRight", false);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
