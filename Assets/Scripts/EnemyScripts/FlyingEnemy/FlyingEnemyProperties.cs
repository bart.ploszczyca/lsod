﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemyProperties : MonoBehaviour
{
    private float enemyHealth;
    private Animator animator;

    void Start()
    {
        animator = this.GetComponent<Animator>();
        enemyHealth = 3.0f;

        animator.SetFloat("StartingPosX", transform.position.x);
        animator.SetFloat("StartingPosY", transform.position.y);
    }
    void Update()
    {
        DestroyWhenKilled();
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Projectile")
        {
            TakeDamage();
        }
    }
    void TakeDamage()
    {
        enemyHealth -= 1.0f;
    }
    void DestroyWhenKilled()
    {
        if(enemyHealth < 0)
        {
            Destroy(gameObject);
        }
    }
}
