﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemySearch : StateMachineBehaviour
{
    public float setTurnsToEndSequence;
    private float turnsToEndSequence;

    public float waitingTime;
    private float runnningTime;

    private bool timerIsRunning = false;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger("playerNotFound");
        turnsToEndSequence = setTurnsToEndSequence;
        runnningTime = waitingTime;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        SearchSequence(animator);
        WaitingTimer(animator);
    }

    void SearchSequence(Animator animator)
    {
        if (turnsToEndSequence == 0)
        {
            animator.SetTrigger("playerNotFound");
        }
        else if (turnsToEndSequence > 0)
        {
            timerIsRunning = true;
        }
    }
    void WaitingTimer(Animator animator)
    {
        if (timerIsRunning)
        {
            if (runnningTime > 0)
            {
                runnningTime -= Time.deltaTime;
            }
            else
            {
                runnningTime = waitingTime;
                timerIsRunning = false;
                turnsToEndSequence -= 1.0f;
                if (turnsToEndSequence > 0)
                    TurnEnemy(animator);
            }
        }
    }
    void TurnEnemy(Animator animator)
    {
            if (animator.GetBool("isFacingRight"))
            {
                animator.transform.eulerAngles = new Vector3(0, -180, 0);
                animator.SetBool("isFacingRight", false);
            }
            else if (!animator.GetBool("isFacingRight"))
            {
                animator.transform.eulerAngles = new Vector3(0, 0, 0);
                animator.SetBool("isFacingRight", true);
            }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
