﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemyAttack : StateMachineBehaviour
{
    public float speed;
    public float stoppingDistance;
    public float retreatDistance;

    public float timeBtwShots;
    private float timeleftBtwShots;
    private Transform LBSpawnPoint;
    public GameObject projectile;
    private Vector2 enemyPos;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        LBSpawnPoint = animator.transform.GetChild(2).transform;
        timeleftBtwShots = timeBtwShots;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemyPos = animator.transform.position;
        

        AttackMovement(animator);
        AttackFrequency(animator);
        TurnEnemyIfFacingWrong(animator);
    }
    void AttackMovement(Animator animator)
    {
        var target = PlayerController.instance;
        Vector2 targetPos = target.gameObject.transform.position;

        if (Vector2.Distance(enemyPos, targetPos) > stoppingDistance)
        {
            //Debug.Log("kurwa1");
            animator.transform.position = Vector2.MoveTowards(enemyPos, targetPos, speed * Time.deltaTime);
        }
        else if (Vector2.Distance(enemyPos, targetPos) < stoppingDistance && Vector2.Distance(enemyPos, targetPos) > retreatDistance)
        {
            //Debug.Log("kurwa2");
        }
        else if (Vector2.Distance(enemyPos, targetPos) < retreatDistance)
        {
            //Debug.Log("kurwa3");
            animator.transform.position = Vector2.MoveTowards(enemyPos, targetPos, -speed * Time.deltaTime);
        }
    }
    void TurnEnemyIfFacingWrong(Animator animator)
    {
        var target = PlayerController.instance;
        Vector2 targetPos = target.gameObject.transform.position;

        if (targetPos.x > enemyPos.x && !animator.GetBool("isFacingRight"))
        {
            animator.transform.eulerAngles = new Vector3(0, 0, 0);
            animator.SetBool("isFacingRight", true);
        }
        else if (targetPos.x < enemyPos.x && animator.GetBool("isFacingRight"))
        {
            animator.transform.eulerAngles = new Vector3(0, -180, 0);
            animator.SetBool("isFacingRight", false);
        }
    }
    void AttackFrequency(Animator animator)
    {
        var target = PlayerController.instance;
        Transform targetTransform = target.gameObject.transform;

        if (timeleftBtwShots < 0)
        {
            Instantiate(projectile, LBSpawnPoint.position, Quaternion.identity, LBSpawnPoint);
            timeleftBtwShots = timeBtwShots;
        }
        else
        {
            timeleftBtwShots -= Time.deltaTime;
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
