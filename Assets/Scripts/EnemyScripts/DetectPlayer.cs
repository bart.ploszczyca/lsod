﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectPlayer : MonoBehaviour
{
    private Animator enemyAnimator;
    public CapsuleCollider2D collider;

    void Start()
    {
        enemyAnimator = gameObject.GetComponentInParent<Animator>();
        collider = gameObject.GetComponent<CapsuleCollider2D>();
    }

    void Update()
    {
        if (enemyAnimator.GetBool("playerDetected") == false)
        {
            collider.enabled = true;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            enemyAnimator.SetBool("playerDetected", true);
            collider.enabled = false;
        }
    }
}
