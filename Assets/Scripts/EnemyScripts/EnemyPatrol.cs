﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : StateMachineBehaviour
{
    private Transform pathDetection;
    int layerMask;

    public float patrolSpeed;
    public float pathDetectionDistance;
    private bool isFacingRight = true;


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        layerMask = LayerMask.GetMask("EnemyPatrolPath");
        pathDetection = animator.transform.GetChild(0);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Patrol(animator);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    void Patrol(Animator animator)
    {
        animator.transform.Translate(Vector2.right * patrolSpeed * Time.deltaTime);

        RaycastHit2D pathInfo = Physics2D.Raycast(pathDetection.position, Vector2.down, pathDetectionDistance, layerMask);
        if (pathInfo.collider == false)
        {
            if (isFacingRight)
            {
                animator.transform.eulerAngles = new Vector3(0, -180, 0);
                isFacingRight = false;
            }
            else
            {
                animator.transform.eulerAngles = new Vector3(0, 0, 0);
                isFacingRight = true;
            }
        }
    }


    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
